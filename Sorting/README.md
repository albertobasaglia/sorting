# Sorting
### Versione JDK: 10.0.1
Il programma permette un confronto nei tempi di esecuzione dei vari algoritmi di ordinamento.

Oltre agli algoritmi é presente anche il confronto tra un'esecuzione non concorrente ed una concorrente con 2+ threads.
## GUI e CLI
### Gui
![GUI](https://i.imgur.com/XOHMrZM.png)
La versione GUI (con entry point nella classe main di gui.Main) consente un facile utilizzo delle funzioni del programma.

Dal qui é possibile creare un "test" a propria discrezione.

Sará possibile difatti scegliere l'algoritmo di ordinamento da usare, il numero degli elementi del vettore e i threads che verranno impiegati dal programma per ordinare il vettore

Il pulsante "AGGIUNGI" serve per aggiungere un test

Il pulsante "CANCELLA" serve per rimuovere il test selezionato

Per motivi di stabilitá e di affidibilitá dei risultati é possibile eseguire solo un test alla volta. 

In caso contrario il programma mostra un Dialog di errore all'utente
### CLI
![CLI](https://i.imgur.com/T23zK5f.png)

Nel programma é presente anche una versione a riga di comando che esegue solamente un confronto.

L'entry point é nella classe model.Collaudo.

## Struttura del programma
### Ordinamenti
Il programma é flessibile per quanto riguarda i metodi di ordinamento , essi difatti devono solo implementare l'interfaccia Ordinamento per essere utilizzati nel codice.
```java
package ordinamento;
public interface Ordinamento {
    void ordina(int[] vettore);
}
```
### Metodo di esecuzione
1. Viene salvato il tempo in ms del sistema
2. Viene generato il vettore secondo i criteri decisi dall'utente
3. Se l'utente ha deciso di ordinare con un thread, viene fatto l'ordinamento e si passa al punto 6, se l'utente utilizza piú di un thread il vettore viene diviso in tante parti tanti sono i thread.
4. Ad ogni thread viene affidato il compito di ordinare una delle parti
5. Quando tutti i thread hanno finito, viene fatto il merge di tutti gli array ordinati , ottenendo cosí il vettore di partenza ordinato
6. Si sottrae al tempo del sistema corrente il tempo salvato al punto 1 , ottenendo cosí il tempo che il programma ha impiegato per ordinare il vettore

## Metodi utilizzati

### Implementazioni di Runnable

### ConcorrenteRunnable:
Si occupa di:
1. Dividere l'array
2. Far partire gli OrdinamentoRunner
3. Aspettare che finiscano
4. Fare il merge degli array risultanti
5. Calcolare il tempo impiegato

Questa é la parte principale del codice in cui é contenuta la logica fondamentale
```java
long startTime = System.currentTimeMillis();
/*
* 1. Dividere l'array
*/
int[][] vettori;
vettori = new int[nThreads][];
for(int i=0 ; i<nThreads ; i++) {
	int dim = (n/nThreads);
	int start = dim * i;
	int end = dim * (i+1);
	vettori[i] = new int[n/nThreads];
	for(int k=start,index =0 ; k<end ; k++,index++) {
		vettori[i][index] = vett[k];
	}
}
/*
* 2. Far partire gli OrdinamentoRunner
*/
Thread[] threads = new Thread[nThreads];
for(int i=0 ; i<nThreads ; i++) {
	threads[i] = new Thread(new OrdinamentoRunner(ordinamento,vettori[i]));
	threads[i].start();
}
/*
* 3. Aspettare che finiscano
*/
for(Thread t: threads) {
	try {
		t.join();
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
}
/*
* 4. Fare il merge degli array risultanti
*/
int[] merged = ArrayFactory.merge(vettori);
/*
* 5. Calcolare il tempo impiegato
*/
long time = System.currentTimeMillis() - startTime;

```

Questo Runnable é necessario solo nella versione GUI in quanto aspettare un Thread (t.join()) dal Thread di JavaFX causerebbe il blocco dell'interfaccia grafica. 
### OrdinamentoRunner:
Si occupa di:
1. Eseguire l'ordinamento di un array dato

