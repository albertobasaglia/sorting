package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Controller controller = new Controller();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gui.fxml"));
        loader.setController(controller);
        loader.load();
        VBox root = loader.getRoot();
        Scene s = new Scene(root,1024,600);
        primaryStage.setMinWidth(1024);
        primaryStage.setMinHeight(600);
        primaryStage.setTitle("Comparazione Algoritmi");
        primaryStage.setScene(s);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
        primaryStage.setOnCloseRequest(event -> {
            if(controller.isRunning()){
                final Stage dialog = new Stage();
                dialog.initModality(Modality.APPLICATION_MODAL);
                dialog.initOwner(primaryStage);
                VBox dialogVbox = new VBox(20);
                Label l = new Label("Ci sono degli ordinamenti in esecuzione, aspetta che finiscano!");
                l.setWrapText(true);
                //l.setPadding(new Insets(5,5,0,5));
                dialogVbox.getChildren().add(l);
                Button force = new Button("Force Kill");
                dialogVbox.getChildren().add(force);
                force.setOnAction(event1 -> {
                    System.exit(0);
                });
                Scene dialogScene = new Scene(dialogVbox, 200, 100);
                dialog.setScene(dialogScene);
                dialog.show();
                dialog.setResizable(false);
                dialog.setTitle("Errore");
                event.consume();
            } else {
                System.exit(0);
            }
        });
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
