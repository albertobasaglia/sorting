/**
 * Controller dell'interfaccia principale di javaFX
 * @author Alberto Basaglia
 * */
package gui;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.TipoOrdine;
import model.ordinamenti.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    TableView<Elemento> table;
    @FXML
    Button bottone;
    @FXML
    Button bottoneCancella;
    @FXML
    ComboBox algoritmi;
    @FXML
    ComboBox elementi;
    @FXML
    ComboBox threads;
    @FXML
    CheckBox ordinato;
    @FXML
    CheckBox contrario;
    private ObservableList<Elemento> lista = FXCollections.observableArrayList();
    private ObservableList<Ordinamento> listaAlgoritmi;
    private ObservableList<Integer> listaElementi;
    private ObservableList<Integer> listaThreads;
    //private static boolean running = false;
    private static BooleanProperty running;
    private static Elemento selezionato;
    private ObservableList selectedCells;
    public Controller() {
        running = new SimpleBooleanProperty(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //ArrayList osservabili per le ComboBox
        bottoneCancella.setDisable(true);
        bottone.disableProperty().bind(running);
        contrario.visibleProperty().bind(ordinato.selectedProperty());
        listaAlgoritmi = FXCollections.observableArrayList();
        listaElementi = FXCollections.observableArrayList();
        listaThreads = FXCollections.observableArrayList();
        algoritmi.setItems(listaAlgoritmi);
        elementi.setItems(listaElementi);
        threads.setItems(listaThreads);
        //Aggiunta degli elementi alle ComboBox
        listaAlgoritmi.addAll(new SelectionSort(),new BubbleSort(),new InsertionSort(),new ShellSort(),new MergeSort());
        listaElementi.addAll(4096,8192,16384,32768,65536,131072,262144,524288,1048576,
                2097152,4194304,8388608);
        listaThreads.addAll(1,2,4,8,16,32,64,128,256,512);
        //Valori di default per le ComboBox
        algoritmi.setValue(new SelectionSort());
        elementi.setValue(4096);
        threads.setValue(1);
        //Azione del bottone di aggiunta Elemento
        bottone.setOnAction(event -> {
            TipoOrdine ordine;
            if(ordinato.isSelected()) {
                if(contrario.isSelected()) ordine = TipoOrdine.ORDINATO_AL_CONTRARIO;
                else ordine = TipoOrdine.ORDINATO;
            } else {
                ordine = TipoOrdine.CASUALE;
            }
            Elemento nuovo = new Elemento((Integer) threads.getValue(),(Integer) elementi.getValue(),(Ordinamento) algoritmi.getValue(),ordine);
            lista.add(nuovo);
            nuovo.esegui();
            running.set(true);
        });
        bottoneCancella.setOnAction(event -> {
            lista.remove(selezionato);
        });
        table.setItems(lista);
        //Aggiunta Colonne nella TableView
        TableColumn<Elemento,String> colAlgoritmo = new TableColumn<>("Algoritmo");
        colAlgoritmo.setCellValueFactory(new PropertyValueFactory<>("ordinamento"));
        colAlgoritmo.setMinWidth(100);
        TableColumn<Elemento,Integer> colMetodo = new TableColumn<>("Threads");
        colMetodo.setCellValueFactory(new PropertyValueFactory<>("threads"));
        colMetodo.setMinWidth(40);
        TableColumn<Elemento,Integer> colElementi = new TableColumn<>("Elementi");
        colElementi.setCellValueFactory(new PropertyValueFactory<>("elementi"));
        colElementi.setMinWidth(100);

        TableColumn<Elemento,TipoOrdine> colOrdine = new TableColumn<>("Giá ordinato");
        colOrdine.setCellValueFactory(new PropertyValueFactory<>("ordine"));
        colOrdine.setMinWidth(200);
        colOrdine.setCellFactory(col-> new TableCell<>() {
            @Override
            protected void updateItem(TipoOrdine item, boolean empty) {
                super.updateItem(item, empty);
                if(!empty) {
                    String text="";
                    switch(item) {
                        case CASUALE:
                            text = "NO";
                            break;
                        case ORDINATO:
                            text = "SI";
                            break;
                        case ORDINATO_AL_CONTRARIO:
                            text = "AL CONTRARIO";
                            break;
                    }
                    setText(text);
                } else {
                    setText(null);
                }
            }
        });

        TableColumn<Elemento,String> colStato = new TableColumn<>("Stato");
        colStato.setCellValueFactory(new PropertyValueFactory<>("stato"));
        colStato.setMinWidth(200);
        //Aggiunta di tutte le colonne alla TableView
        table.getColumns().addAll(colAlgoritmo,colMetodo,colElementi,colOrdine,colStato);
        selectedCells = table.getSelectionModel().getSelectedCells();

        selectedCells.addListener((ListChangeListener) c -> updateElemento());
        running.addListener(event -> updateElemento());
    }
    private void updateElemento() {
        if(selectedCells.size()>0) {
            TablePosition tablePosition = (TablePosition) selectedCells.get(0);
            selezionato = lista.get(tablePosition.getRow());
            if(selezionato.isRunning()) {
                bottoneCancella.setDisable(true);
            } else {
                bottoneCancella.setDisable(false);
            }
        } else {
            selezionato = null;
            bottoneCancella.setDisable(true);
        }
    }

    public static boolean isRunning() {
        return running.get();
    }

    public static BooleanProperty runningProperty() {
        return running;
    }

    public static void setRunning(boolean running) {
        Controller.running.set(running);
    }
}
