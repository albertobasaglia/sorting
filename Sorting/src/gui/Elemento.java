/**
 * Elemento che descrive una linea della TableView
 * @author Alberto Basaglia
 *
 * */
package gui;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import model.ConcorrenteRunnable;
import model.TipoOrdine;
import model.ordinamenti.Ordinamento;

public class Elemento {
    private Integer threads; //Threads che eseguono l'ordinamento
    private Integer elementi; //Elementi che compongono il vettore
    private Ordinamento ordinamento; //Tipo di ordinamento
    private TipoOrdine ordine; //Ordine del vettore
    private ReadOnlyStringWrapper stato; //Stato dell'esecuzione
    private boolean running;
    public Elemento(Integer threads, Integer elementi, Ordinamento ordinamento, TipoOrdine ordine) {
        this.threads = threads;
        this.elementi = elementi;
        this.ordinamento = ordinamento;
        this.ordine = ordine;
        this.stato = new ReadOnlyStringWrapper("Non ancora eseguito");
        this.running = false;
    }
    public void esegui() {
        this.stato.set("Lanciato!");
        ConcorrenteRunnable cr = new ConcorrenteRunnable(ordinamento,elementi,this,threads,ordine);
        Thread t = new Thread(cr);
        t.start();
        this.running = true;
    }
    public Integer getElementi() {
        return elementi;
    }

    public void setElementi(Integer elementi) {
        this.elementi = elementi;
    }

    public Ordinamento getOrdinamento() {
        return ordinamento;
    }

    public void setOrdinamento(Ordinamento ordinamento) {
        this.ordinamento = ordinamento;
    }

    public ReadOnlyStringProperty statoProperty() {
        return stato.getReadOnlyProperty();
    }

    public ReadOnlyStringWrapper getStato() {
        return stato;
    }

    public Integer getThreads() {
        return threads;
    }

    public void setThreads(Integer threads) {
        this.threads = threads;
    }

    public TipoOrdine getOrdine() {
        return ordine;
    }

    public void setOrdine(TipoOrdine ordine) {
        this.ordine = ordine;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
