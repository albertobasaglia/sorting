package model;

import model.ordinamenti.Ordinamento;

public class OrdinamentoRunner implements Runnable{
    private Ordinamento ordinamento;
    private int[] vettore;
    public OrdinamentoRunner(Ordinamento ordinamento, int[] vettore) {
        this.ordinamento = ordinamento;
        this.vettore = vettore;
    }

    @Override
    public void run() {
        ordinamento.ordina(vettore);
    }
}
