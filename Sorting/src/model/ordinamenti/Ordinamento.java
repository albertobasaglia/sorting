/**
 * Interfaccia funzionale per le classi che implementano un metodo di ordinamento
 * @author Alberto Basaglia
 * */
package model.ordinamenti;
public interface Ordinamento {
    /**
     * Metodo per l'ordinamento di un vettore
     * @param vettore vettore di interi da ordinare
     * */
    void ordina(int[] vettore);
}
