/**
 * Classe che implementa Ordinamento che esegue un Shell Sort
 * @author Alberto Basaglia
 * */
package model.ordinamenti;

public class ShellSort implements Ordinamento{
    @Override
    public void ordina(int[] vettore) {
        int n = vettore.length;
        for (int gap = n/2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < n; i += 1)
            {
                int temp = vettore[i];
                int j;
                for (j = i; j >= gap && vettore[j - gap] > temp; j -= gap)
                    vettore[j] = vettore[j - gap];
                vettore[j] = temp;
            }
        }
    }

    @Override
    public String toString() {
        return "Shell Sort";
    }
}
