/**
 * Classe che implementa Ordinamento che esegue un Bubble Sort
 * @author Alberto Basaglia
 * */
package model.ordinamenti;

public class BubbleSort implements Ordinamento {
    @Override
    public void ordina(int[] vettore) {
        int n = vettore.length;
        int temp = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(vettore[j-1] > vettore[j]){
                    temp = vettore[j-1];
                    vettore[j-1] = vettore[j];
                    vettore[j] = temp;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Bubble Sort";
    }
}
