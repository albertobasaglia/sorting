/**
 * Classe che implementa Ordinamento che esegue un Selection Sort
 * @author Alberto Basaglia
 * */
package model.ordinamenti;

public class SelectionSort implements Ordinamento {
    /**
     * Metodo per eseguire un Selection Sort su un vettore dato
     * @param vettore vettore da ordinare
     * */
    @Override
    public void ordina(int[] vettore) {
        for(int i=0 ; i<vettore.length - 1 ; i++) {
            int posMin = i;
            for(int k=i+1 ; k<vettore.length ; k++) {
                if(vettore[k]<vettore[posMin]) {
                    posMin = k;
                }
            }
            if(posMin!=i) {
                int supporto = vettore[posMin];
                vettore[posMin] = vettore[i];
                vettore[i] = supporto;
            }
        }
    }

    @Override
    public String toString() {
        return "Selection Sort";
    }
}
