/**
 * Classe che implementa Ordinamento che esegue un Insertion Sort
 * @author Alberto Basaglia
 * */
package model.ordinamenti;

public class InsertionSort implements Ordinamento {
    @Override
    public void ordina(int[] vettore) {
        int temp;
        for (int i = 1; i < vettore.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(vettore[j] < vettore[j-1]){
                    temp = vettore[j];
                    vettore[j] = vettore[j-1];
                    vettore[j-1] = temp;
                }
            }
        }
    }

    @Override
    public String toString() {
        return "Insertion Sort";
    }
}
