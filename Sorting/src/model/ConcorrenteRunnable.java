package model;

import gui.Controller;
import gui.Elemento;
import model.ordinamenti.Ordinamento;

public class ConcorrenteRunnable implements Runnable{
    private Ordinamento ordinamento;
    private Integer n;
    private Elemento lanciatore;
    private Integer nThreads;
    private TipoOrdine ordine;

    public ConcorrenteRunnable(Ordinamento ordinamento, Integer n, Elemento lanciatore,Integer nThreads,TipoOrdine ordine) {
        this.ordinamento = ordinamento;
        this.n = n;
        this.lanciatore = lanciatore;
        this.nThreads = nThreads;
        this.ordine = ordine;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        int[] vett;
        switch(ordine) {
            case CASUALE: vett = ArrayFactory.generateRandomInteger(n,n);break;
            case ORDINATO: vett = ArrayFactory.generateOrderedInteger(n);break;
            case ORDINATO_AL_CONTRARIO: vett = ArrayFactory.generateReversedOrderedInteger(n);break;
            default: vett = ArrayFactory.generateRandomInteger(n,n);break;
        }
        if(nThreads > 1) {
            int[][] vettori;
            vettori = new int[nThreads][];
            for(int i=0 ; i<nThreads ; i++) {
                int dim = (n/nThreads);
                int start = dim * i;
                int end = dim * (i+1);
                vettori[i] = new int[n/nThreads];
                for(int k=start,index =0 ; k<end ; k++,index++) {
                    vettori[i][index] = vett[k];
                }
            }
            Thread[] threads = new Thread[nThreads];
            for(int i=0 ; i<nThreads ; i++) {
                threads[i] = new Thread(new OrdinamentoRunner(ordinamento,vettori[i]));
                threads[i].start();
            }
            for(Thread t: threads) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int[] merged = ArrayFactory.merge(vettori);
        } else {
            ordinamento.ordina(vett);
        }
        long time = System.currentTimeMillis() - startTime;
        lanciatore.getStato().set("Finito in "+time+"ms");
        Controller.runningProperty().set(false);
        lanciatore.setRunning(false);
    }
}