/**
 * Classe contenente metodi statici utili alla generazione di array
 * @author Alberto Basaglia
 * */
package model;
import java.util.PriorityQueue;
import java.util.Random;
public class ArrayFactory {
    static Random random = new Random();
    /**
     * Metodo per la generazione casuale di un array
     * @param size dimensione dell'array
     * @param max numero massimo generato (escluso)
     * @return array generato casualmente
     * */
    static int[] generateRandomInteger(int size,int max) {
        int[] ret = new int[size];
        for(int i=0; i<size; i++) {
            ret[i] = random.nextInt(max);
        }
        return ret;
    }
    /**
     * Metodo per la generazione di un array giá ordinato
     * @param size dimensione dell'array
     * @return array generato
     * */
    static int[] generateOrderedInteger(int size) {
        int[] ret = new int[size];
        for(int i=0 ; i<size ; i++) {
            ret[i] = i;
        }
        return ret;
    }
    /**
     * Metodo per la generazione di un array giá ordinato
     * @param size dimensione dell'array
     * @return array generato
     * */
    static int[] generateReversedOrderedInteger(int size) {
        int[] ret = new int[size];
        for(int i=0 ; i<size ; i++) {
            ret[size-i-1] = i;
        }
        return ret;
    }
    public static int[] merge(int[][] arr) {
        PriorityQueue<ArrayContainer> queue = new PriorityQueue<>();
        int total=0;
        for (int i = 0; i < arr.length; i++) {
            queue.add(new ArrayContainer(arr[i], 0));
            total = total + arr[i].length;
        }
        int m=0;
        int result[] = new int[total];
        while(!queue.isEmpty()){
            ArrayContainer ac = queue.poll();
            result[m++]=ac.arr[ac.index];
            if(ac.index < ac.arr.length-1){
                queue.add(new ArrayContainer(ac.arr, ac.index+1));
            }
        }
        return result;
    }
}
