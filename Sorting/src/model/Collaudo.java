package model;

import model.ordinamenti.SelectionSort;

public class Collaudo {
    public static void main(String[] args) {
        int size = 128000;
        if(args.length==1) {
            try{
                int val = Integer.parseInt(args[0]);
                if(val%2==1) {
                    System.out.printf("Impossibile dividere un array di elementi dispari in due, usando valore di default.(%d)\n",size);
                } else {
                    size = val;
                }
            } catch(NumberFormatException e) {
                System.out.printf("Numero non valido, usando valore di default.(%d)\n",size);
            }
        }
        int[] vett1 = ArrayFactory.generateRandomInteger(size,10000);
 	int[] vett2 = new int[size];
	for(int i=0 ; i<size ; i++) {
		vett2[i] = vett1[i];
	}
        System.out.printf("Inizio ordinamento di %d elementi con 1 Thread...\n",size);
        long start = System.currentTimeMillis();
        SelectionSort sr = new SelectionSort();
        sr.ordina(vett1);
        long seconds = System.currentTimeMillis() - start;
        System.out.printf("Ci sono voluti %d millisecondi\n",seconds);
        System.out.printf("Inizio ordinamento di %d elementi con 2 Thread...\n",size);
        start = System.currentTimeMillis();
        int half = size/2;
        int[] vettA = new int[half];
        int[] vettB = new int[half];
        for(int i=0 ; i<half ; i++) {
            vettA[i] = vett2[i];
        }
        for(int i=0 ; i+half<size ; i++) {
            vettB[i] = vett2[i+half];
        }
        OrdinamentoRunner ordinamentoA = new OrdinamentoRunner(new SelectionSort(),vettA);
        OrdinamentoRunner ordinamentoB = new OrdinamentoRunner(new SelectionSort(),vettB);
        Thread solverA = new Thread(ordinamentoA);
        Thread solverB = new Thread(ordinamentoB);
        solverA.start();
        solverB.start();
        try {
            solverA.join();
            solverB.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finiti ordinamenti...\nInizio merge...");
        int[][] vettori = new int[2][];
        vettori[0] = vettA;
        vettori[1] = vettB;
        vett2 =  ArrayFactory.merge(vettori);
        seconds = System.currentTimeMillis() - start;
        System.out.printf("Ci sono voluti %d millisecondi\n",seconds);
        for(int i=0 ; i<size ; i++) {
            if(vett1[i] != vett2[i]) {
                System.out.println("I vettori NON sono uguali");
                return;
            }
        }
        System.out.println("I vettori sono uguali");
    }
}
