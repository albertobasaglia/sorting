package model;

public enum TipoOrdine {
    CASUALE,
    ORDINATO,
    ORDINATO_AL_CONTRARIO
}
