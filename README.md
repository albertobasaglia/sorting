# Sorting concorrente

Si tenti di confrontare per un array sufficientemente lungo l'ordinamento mediante algoritmo noto
in modo concorrente (2 thread che lavorano in parallelo sulle metà dell'array) e in modo non concorrente.

## Valutazione

Oltre alla pulizia del codice, alla documentazione ed alle scelte operate il progetto
deve essere consegnato in una cartella `Sorting` che contenga il file `README.md` in cui si dà breve descrizione del
progetto con attenzione alle scelte operate. Le valutazioni eccellenti (8, 9, 10) deriveranno da un consistente apporto personale e creativo.
